# st45: private Social Tools
st45 は tmd45 が個人利用のために作成したソーシャルサービス利用ツール群です。勉強がてら作成しておりますのでツッコミや fork などご随意に。

* 主な言語：PHP, JavaScript
* フレームワーク：CodeIgniter & CI_Smarty


## MEMO

* 作成中に気になった点や問題点、解決方法はできるだけ issues にメモする。
* TIPS や参考になる情報は Wiki にメモする。

あとでブログのネタにして消化すると better.
